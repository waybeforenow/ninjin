#include "../resources.hpp"

#include <boost/ut.hpp>
#include <stdint.h>

using namespace boost::ut;

suite resources = [] {
    "init a resource bag"_test = [] {
        Resources<uint16_t> r;
        r.load(420, "funny number");
        expect(eq(*(r.get("funny number").get()), 420));
    };
};
