#include "../sfml.hpp"
#include "../ninjin.hpp"
#include "../resources.hpp"
#include "lager/store.hpp"
#include "lager/util.hpp"
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <boost/ut.hpp>
#include <functional>
#include <memory>
#include <optional>
#include <string>
#include <unordered_map>
#include <utility>
#include <variant>

struct Intro
{
    size_t ticks = 0;
};
struct Game
{
};
struct GameOver
{
};

using TestState = std::variant<Intro, Game, GameOver>;

struct TestModel
{
    TestState state = Intro{};
    int counter = 0;
};

struct TestInc
{
    size_t amount;
};

struct TestDec
{
    size_t amount;
};

struct TestNothing
{
};

using TestAction = std::variant<TestInc, TestDec, TestNothing>;

template <typename T> TestState reduce(T g, TestAction a);

template <> TestState reduce(Intro g, TestAction a)
{
    return std::visit(lager::visitor{[&](TestNothing) {
                                         if (++g.ticks > 60 * 5)
                                         {
                                             return TestState(Game{});
                                         }
                                         return TestState(g);
                                     },
                                     [&](auto) { return TestState(g); }},
                      a);
}

template <> TestState reduce(Game g, TestAction a)
{
    return g;
}
template <> TestState reduce(GameOver g, TestAction a)
{
    return g;
}

TestModel TestReducer(TestModel g, TestAction a)
{
    g.state = std::visit([&](auto &&state) { return reduce(state, a); }, g.state);
    return g;
}

template <typename T> void draw(T state, sf::RenderTarget &target);

template <> void draw(Intro, sf::RenderTarget &target)
{
    const auto font = getResources<sf::Font>().get("SourceSansPro-Regular.ttf");
    sf::Text text("Fucko Games", *font);
    target.draw(text);
}

template <> void draw(Game, sf::RenderTarget &target)
{
}

template <> void draw(GameOver, sf::RenderTarget &target)
{
}

using namespace boost::ut;

suite sfml = [] {
    "init"_test = [] {
        should("start Ninjin with SFML component bundle") = [] {
            getResources<sf::Font>().loadFromFile("../vendor/lager/resources/SourceSansPro-Regular.ttf",
                                                  "SourceSansPro-Regular.ttf");

            const auto event = [&](const sf::Event &ev) -> std::pair<bool, std::optional<TestAction>> {
                switch (ev.type)
                {
                case sf::Event::Closed:
                    return std::make_pair(false, std::nullopt);
                default:
                    break;
                }
                return std::make_pair(true, std::nullopt);
            };

            const auto tick = [&]() -> TestAction { return TestNothing{}; };

            auto my_bundle = NinjinBundle<SfmlView, SfmlEventLoop, TestModel, TestAction, const sf::Event &>{
                SfmlDraw<TestModel>, TestReducer, event, tick};
            NinjinInit(std::move(my_bundle), 320, 240, "Test");
            expect(true);
        };
    };
};
