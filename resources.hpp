#pragma once

#include <memory>
#include <string>
#include <unordered_map>

struct ResourceError : public std::exception
{
    std::string m_what;

    ResourceError(const std::string &resource_key) : m_what(resource_key)
    {
    }

    const char *what() const throw()
    {
        return m_what.c_str();
    }
};

template <typename T> class Resources
{
  private:
    std::unordered_map<std::string, std::shared_ptr<T>> m_resources;

  public:
    Resources<T>() = default;
    Resources<T>(const Resources<T> &) = delete;
    void operator=(const Resources<T> &) = delete;

    void load(T &&val, std::string key)
    {
        m_resources[key] = std::make_shared<T>(std::move(val));
    }

    void loadFromMemory(const void *data, size_t size, std::string key)
    {
        m_resources[key] = std::make_shared<T>();
        if (!m_resources.at(key)->loadFromMemory(data, size))
        {
            throw ResourceError(key);
        }
    }

    void loadFromFile(std::string file, std::string key)
    {
        m_resources[key] = std::make_shared<T>();
        if (!m_resources.at(key)->loadFromFile(file))
        {
            throw ResourceError(key);
        }
    }

    std::shared_ptr<T> get(std::string key)
    {
        return m_resources.at(key);
    }
};

template <typename... Ts> using ResourceBag = std::tuple<Resources<Ts>...>;

template <typename T> static Resources<T> &getResources()
{
    static Resources<T> instance;
    return instance;
}
