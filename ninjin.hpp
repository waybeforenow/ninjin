#pragma once

#include "lager/store.hpp"

template <typename _ViewType, typename _WithEventLoopType, typename _ModelType, typename _ActionType,
          typename _EventType>
struct NinjinBundle
{
    using ViewType = _ViewType;
    using WithEventLoopType = _WithEventLoopType;
    using ModelType = _ModelType;
    using ActionType = _ActionType;

    std::function<void(ViewType &, ModelType)> Draw;
    std::function<_ModelType(_ModelType, _ActionType)> Reducer;
    std::function<std::pair<bool, std::optional<_ActionType>>(_EventType)> EventHandler;
    std::function<_ActionType(void)> TickHandler;
};

template <typename Bundle> void NinjinInit(Bundle bundle, size_t width, size_t height, const char *title)
{
    typename Bundle::ViewType view(width, height, title);
    typename Bundle::WithEventLoopType loop(std::move(view));
    typename Bundle::ModelType model;
    auto store = lager::make_store<typename Bundle::ActionType>(model, loop, lager::with_reducer(bundle.Reducer));

    lager::watch(store, [&](auto &&model) { bundle.Draw(view, LAGER_FWD(model)); });
    loop.run(LAGER_FWD(store), LAGER_FWD(bundle.EventHandler), LAGER_FWD(bundle.TickHandler));
}
