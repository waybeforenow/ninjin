#pragma once

#include <functional>
#include <memory>
#include <optional>
#include <variant>

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

/**
 * View class for an SFML RenderWindow
 */
class SfmlView
{
  public:
    std::unique_ptr<sf::RenderWindow> m_window;

    SfmlView(size_t width, size_t height, const char *title)
        : m_window(std::make_unique<sf::RenderWindow>(
              sf::VideoMode{static_cast<unsigned int>(width), static_cast<unsigned int>(height)}, title))
    {
        this->m_window->setVerticalSyncEnabled(true);
    }

    SfmlView(const SfmlView &) = delete;
    SfmlView(SfmlView &&other) : m_window(std::move(other.m_window))
    {
    }
};

/**
 * Event loop hooks for `lager` to read events from our `SfmlView`
 */
struct SfmlEventLoop
{
    std::reference_wrapper<SfmlView> m_view;

    SfmlEventLoop(SfmlView &&view) : m_view(view)
    {
    }

    template <typename StoreType, typename EventType, typename ActionType>
    void run(StoreType &&store, std::function<std::pair<bool, std::optional<ActionType>>(EventType)> &&handler,
             std::function<ActionType()> &&tick)
    {
        using Result = std::pair<bool, std::optional<ActionType>>;

        bool keep_running = true;
        sf::Event event;
        while (keep_running)
        {
            Result res = (m_view.get().m_window->pollEvent(event)) ? handler(event)
                                                                   : std::make_pair(true, std::make_optional(tick()));
            keep_running = res.first;
            if (res.second)
            {
                store.dispatch(*res.second);
            }
        }
    }

    void finish()
    {
    }

    void post(std::function<void()> fn)
    {
        fn();
    }

    void pause()
    {
    }

    void async(std::function<void()>)
    {
        throw std::runtime_error{"not implemented!"};
    }

    void resume()
    {
        throw std::runtime_error{"not implemented!"};
    }
};

/**
 * Generic draw loop for an `SfmlView`.
 */
template <typename Model> void SfmlDraw(SfmlView &v, Model g)
{
    v.m_window->clear();

    std::visit([&](auto &&state) { draw(state, *v.m_window); }, g.state);

    v.m_window->display();
}
